import loadjs from "loadjs"

function register({ registerHook, peertubeHelpers }) {
  initHC(registerHook, peertubeHelpers).catch(err =>
    console.error("Cannot initialize hcaptcha plugin", err)
  )
}

export { register }

function initHC(registerHook, peertubeHelpers) {
  return peertubeHelpers.getSettings().then(async s => {
    if (!s || !s["hcaptcha-site-key"]) {
      console.error(
        "hCaptcha plugin lacks a site key set to use hCaptcha. Falling back to normal registration."
      )
      return
    }

    // add captcha to the first form (user form)
    const node = document.getElementsByTagName("form")[0]
    const div = document.createElement("div")
    div.setAttribute("class", "h-captcha")
    node.appendChild(div)

    // global callback definition is the most flexible way, since
    // loadjs's callback triggers too soon: immediately after the
    // resource load, when in fact api.js loads other resources
    // before it is defining grecaptcha.
    window.onReCaptchaLoadCallback = () => {
      grecaptcha.render(div, {
        callback: () =>
          (window.ReCaptchaLoadCallbackResponse = grecaptcha.getResponse()),
        sitekey: s["hcaptcha-site-key"]
      })
    }
    loadjs(
      "https://hcaptcha.com/1/api.js?onload=onReCaptchaLoadCallback&render=explicit"
    )

    registerHook({
      target: "filter:api.signup.registration.create.params",
      handler: body =>
        Object.assign({}, body, {
          "h-captcha-response": window.ReCaptchaLoadCallbackResponse
        })
    })
  })
}
